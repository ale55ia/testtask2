//
//  ServicesTVC.swift
//  AppServices
//
//  Created by Apple on 02/02/2019.
//  Copyright © 2019 Maria. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class ServicesTVC: UITableViewController
{

    var array: [ServiceModel] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        Alamofire.request("http://job.softinvent.ru/services.php?os=ios").responseJSON { [weak self] response in
            
            if let json = response.result.value as? [AnyObject]
            {
                
                for dictObject  in json
                {
                    let jsonModel = dictObject as! [String:String]
                    self?.array.append(ServiceModel.init(json:jsonModel))
                }
                 self?.tableView.reloadData()
            }
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return array.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {

        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCellIdentifier", for: indexPath) as! ServiceCell
        
        cell.serviceTitle.text = array[indexPath.row].serviceTitle
      
        let url = URL(string: array[indexPath.row].serviceIcon!)!
        let placeholderImage = UIImage(named: "placeholder")!
        
        cell.serviceImgView.af_setImage(withURL: url, placeholderImage: placeholderImage)
        return cell
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
     
        tableView.deselectRow(at: indexPath, animated: true)
        
            if let url = URL(string: array[indexPath.row].serviceLink!)
            {
                if #available(iOS 10.0, *)
                {
                    UIApplication.shared.open(url, options: [:])
                }
                else
                {
                    // Fallback on earlier versions
                }
            }
    }
}
