//
//  RequestVC.swift
//  AppServices
//
//  Created by Apple on 02/02/2019.
//  Copyright © 2019 Maria. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import netfox

extension UIImage
{
    func resized(withPercentage percentage: CGFloat) -> UIImage?
    {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    func resized(toWidth width: CGFloat) -> UIImage?
    {
        let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    func resize(height: Int, width: Int)-> UIImage?
    {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), false, 0)
        self.draw(in: CGRect(origin: .zero, size: CGSize(width: width, height: height)))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage
    }
    func toBase64() -> String?
    {
        guard let imageData = self.pngData() else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
}

class RequestVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    // UI property
    @IBOutlet weak var textField:   UITextField!
    @IBOutlet weak var textView:    UITextView!
    @IBOutlet weak var photoButton: UIButton!
    
    // Store property
    var imageForSending : UIImage?
    {
        didSet
        {
            if let image = imageForSending
            {
                
                self.imageForSending = resizeImage(image: image, targetSize: CGSize(width:1,height:1))

            }
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupKeyboardDismissRecognizer()
    }
    
    // MARK: Action
    @IBAction func sendButtonAction(_ sender: Any)
    {
        NFX.sharedInstance().start()
        if (validateInfoBeforeSending())
        {
            print("валидацию прошли")
            
            let headers: HTTPHeaders = ["Accept" : "multipart/form-data",
                                        "Content-Type" : "multipart/form-data"]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(self.textField.text!.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "name")
                multipartFormData.append(self.textView.text!.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "comment")
                if ((self.imageForSending) != nil)
                {
                    //let smallPicture = self.imageForSending?.resized(toWidth: 10.0)
                    //multipartFormData.append(self.imageForSending!.pngData()!, withName: "photo")
                    //multipartFormData.append((self.imageForSending?.jpegData(compressionQuality: 0.01)!)!, withName: "photo", mimeType: "image/jpeg")
                    //let data = self.imageForSending?.pngData()
                    
                    let smallPicture = self.imageForSending?.resize(height: 1, width: 1) //Метод Витали
                    multipartFormData.append(smallPicture!.pngData()!, withName: "photo", fileName:"1.txt", mimeType: "image/png")
                    
                    if (self.imageForSending!.pngData()!.isEmpty)
                    {
                        print("Картинки нет")
                    }
                    
                    //multipartFormData.append(self.imageForSending!.toBase64()!.data(using: .ascii)!, withName:"photo", mimeType: "image/png")
                }
                else
                {
                    print("Картинки нет!!!!!")
                }
            }, usingThreshold: UInt64.init(), to: "http://job.softinvent.ru/upload.php", method: .post, headers: headers, encodingCompletion: { result in
                
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress
                        {
                        p in
                        print(p.completedUnitCount, p.totalUnitCount)
                    }
                    
                    upload.responseJSON { [weak self] response in
                        print("Succesfully uploaded \(response)")
                        
                        
                        if let json = response.result.value as? [String : AnyObject]
                        {
                            print("json \(json)")
                            
                            if let statusCode = response.response?.statusCode
                            {
                                print("statusCode \(statusCode)")
                                
                                var alertTitle: String = ""
                                var alertMessage: String = ""
                                
                                if (statusCode == 201)
                                {
//                                    alertTitle = "Success \(statusCode)"
//                                    alertMessage = json["filename"] as! String
//                                    print(response.request?.httpBody)
//                                    print(response.response)
//                                    print(response.description)
//                                    print(response.request)
//                                    print(response.value)
//                                    print(response.data)
//                                    print(response.result)
//                                    print(response.response?.allHeaderFields)
//                                    print(response.error)
//                                    print(response.response.debugDescription)
//                                    print(response)
//                                    print(upload)
//                                    print(upload.request)
//                                    print(upload.response)
                                    print(response.response)
                                    print(response.data)
                                    print(result)
                                    
//                                    print(upload.request?.allHTTPHeaderFields)
//                                    print(upload.response?.allHeaderFields)
                                    
//                                    let responseData = response.result.value as! NSDictionary
//                                    print(responseData)

                                    
                                }
                                else if ((statusCode == 400) || (statusCode == 500))
                                {
                                    alertTitle = "Failed \(statusCode)"
                                    alertMessage = json["error"] as! String
                                }
                                let alertController = UIAlertController(title: alertTitle, message:alertMessage, preferredStyle: .alert)
                                let buttonOKForAlert = UIAlertAction(title: "Okey", style: .default) { (action:UIAlertAction) in
                                    
                                    self?.cleaningForms()
                                }
                                alertController.addAction(buttonOKForAlert)
                                self?.present(alertController, animated: true, completion: nil)
                            }
                        }
                        
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                }
            })
        }
        else
        {
            print("валидацию не прошли")
        }
//        NFX.sharedInstance().stop()
       NFX.sharedInstance().show()
    }
    
    @IBAction func photoButtonAction(_ sender: Any)
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera)
        {
            let cameraVC = UIImagePickerController()
            cameraVC.delegate = self
            cameraVC.sourceType = .camera
            cameraVC.allowsEditing = true
            self.present(cameraVC, animated: true, completion: nil)
        }
    }
    
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   // MARK : UIImagePickerControllerDelegate
 
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
    if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
    //if let image = editingInfo[UIImagePickerControllerOriginalImage] as! UIImage
    {
        
        self.imageForSending = resizeImage(image: image, targetSize: CGSize(width:10,height:10))
        self.photoButton.setBackgroundImage(image, for: .normal)
        self.photoButton.setTitle("", for:  .normal)
    }
    self.dismiss(animated: true, completion: nil)
   }

    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Validate info before sending
    func validateInfoBeforeSending() -> Bool
    {
        var isValid: Bool = true
        
        if (textField.text?.isEmpty)!  { isValid = false }
        if (textView.text?.isEmpty)!   { isValid = false }
        return isValid
    }
   
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: hide keyboard
    func setupKeyboardDismissRecognizer()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    
    // MARK: Clearing forms
    func cleaningForms()
    {
        self.imageForSending = nil
        self.textField.text = ""
        self.textView.text = ""
        self.photoButton.setBackgroundImage(nil, for: .normal)
        self.photoButton.setTitle("Прикрепить фото", for:  .normal)
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Helpers
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage
    {
        let size = image.size

        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        var newSize: CGSize//обрезка до квадратной
        if(widthRatio > heightRatio)
        {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        }
        else
        {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }

        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
   }
}
