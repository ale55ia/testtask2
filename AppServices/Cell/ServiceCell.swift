//
//  ServiceCell.swift
//  AppServices
//
//  Created by Apple on 02/02/2019.
//  Copyright © 2019 Maria. All rights reserved.
//

import UIKit

class ServiceCell: UITableViewCell
{

    @IBOutlet weak var serviceImgView: UIImageView!
    @IBOutlet weak var serviceTitle: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
