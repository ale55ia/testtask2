//
//  ServiceModel.swift
//  AppServices
//
//  Created by Apple on 02/02/2019.
//  Copyright © 2019 Maria. All rights reserved.
//

import UIKit

class ServiceModel:NSObject
{

    let serviceIcon : String?
    let serviceTitle: String?
    let serviceLink : String?
    
    
    init(icon: String, title: String, link: String)
    {
        self.serviceIcon  = icon
        self.serviceTitle = title
        self.serviceLink  = link
    }
    
    init(json:[String:String])
    {
        self.serviceIcon  = json["icon"]
        self.serviceTitle = json["title"]
        self.serviceLink  = json["link"]
    }
}
