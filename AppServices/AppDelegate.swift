//
//  AppDelegate.swift
//  AppServices
//
//  Created by Apple on 02/02/2019.
//  Copyright © 2019 Maria. All rights reserved.
//

import UIKit
import Alamofire


@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        

        let pencilImage = UIImage(named:"request")?.withRenderingMode(.alwaysOriginal)
        let pencilButton =   UITabBarItem(title: "Заявки", image: pencilImage, tag: 1)
        
        let folderImage = UIImage(named:"folder")?.withRenderingMode(.alwaysOriginal)
        let folderButton =   UITabBarItem(title: "Сервисы", image: folderImage, tag: 1)
        
        //UINavigationController(rootViewController: <#T##UIViewController#>)
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let servicesTVC = storyboard.instantiateViewController(withIdentifier: "ServicesTVC")
        let requestVC = storyboard.instantiateViewController(withIdentifier: "RequestVC")

        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [servicesTVC,requestVC]
        
        servicesTVC.tabBarItem = folderButton;
        requestVC.tabBarItem   = pencilButton;
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = tabBarController //UINavigationController(rootViewController: tabBarController)
        window?.makeKeyAndVisible()
        
    
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

